#exercises

- create a query selector to get the RED block content
- create a query selector to get the YEOLLOW block content
- create a query selector to get the BLUE block content
- create a query selector to get the GREEN block content
- create a query selector to get the GRAY block**S** content
- create a query selector to click on the button
- create a query selector to read the textarea context that will show up after clicking the button
